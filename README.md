# Slow Food Germany Location Extractor
Extracts Slow Food Germany locations from the official website, and places them into KML format.

There is a variable in the very top of the script (splitkml) that controls the output: one KML file per different point type (True), or (False) output one large, monolithic KML file.

## Dependencies
* Python 3.x
    * Requests module for session functionality
    * Simplekml for easily building KML files
    * JSON module for JSON-based geodata
    * Regular expressions module to facilitate gleaning address and coordinate information buried messily within the popup attribute
    * Defaultdict lib from the Collections module to facilitate initializing empty dictionaries 
* Also of course depends on official [Slow Food Germany](https://www.slowfood.de/) website.
