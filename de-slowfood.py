#!/usr/bin/python3

import requests
import simplekml
import json
import re
from collections import defaultdict

#The splitkml variable controls the output of the script: one KML file per different point type (True), or (False) output one large, monolithic KML file
splitkml = True
#Aside from conviviums (which are not visitable, set up point types of interest
pointtypes = ['restaurant','event','sponsor','producer']
#Initilaize empty dictionaries for each point type.
# The defaultdict lib was needed here because using `pointsbytype = dict.fromkeys(pointtypes,[])` mysteriously resulted in *all* values being populated when values were populated even to one specific key (e.g. pointsbytepe['restaurant'].append({"foo": "bar"} would be added to the values for ALL the keys)
pointsbytype = defaultdict(list)
#Initialize an empty list to be used to collect dictionaries of each location
pointslist = []

#Set KML schema name
kmlschemaname = "de-slowfood"
#Set page URL
pageURL = "https://www.slowfood.de/@@geolocations?de_only=1"
#Open page URL
openURL = requests.get(pageURL)
#Create json object
jsonpoints = openURL.json()

#Example JSON:{'geometry': {'type': 'Point', 'coordinates': [9.3066906, 51.7088303]}, 'type': 'Feature', 'properties': {'popup': '<div class="stack"><h3>Ölmühle Solling</h3><p><q>BIO-Ölspezialitäten aus Ölsaaten und Nüssen aus anerkannt ökologischem Landbau</q></p><h4>Adresse</h4><p>Höxtersche Str. 3, 37691 Boffzen</p><p>\n          <a target="_blank" rel="noopener noreferrer"\n            href="https://www.slowfood.de/netzwerk/vor-ort/bielefeld_owl/empfehlungen/einkaufen/oelmuehle_solling_gmbh">Weitere Informationen</a></p></div>', 'marker-color': '#548cba', 'description': 'BIO-Ölspezialitäten aus Ölsaaten und Nüssen aus anerkannt ökologischem Landbau', 'producer': True, 'convivium': False, 'sponsor': False, 'content_type': 'slwf.website.producer', 'event': False, 'sponsor_category': '', 'marker-symbol': 'grocery', 'title': 'Ölmühle Solling', 'restaurant': False}},

#Example pointslist entry: {'type': 'producer', 'placename': 'Gasthof Hofmann', 'address': 'Haus Nr. 1, 96181 Rauhenebrach-Schindelsee', 'lat': 49.890065919062366, 'lng': 10.64846439960092}
        
#Method for simplekml boilerplate
def kmlinit(pointtype = kmlschemaname):
    #Initialize all necessary variables as global to be available for use *outside* this method
    global kmlfile, kml, schema
    #Set filename/path for KML file output
    kmlfile = "de-slowfood-" + pointtype + ".kml"
    #Initialize kml object
    kml = simplekml.Kml()
    #Add schema, which is required for a custom address field
    schema = kml.newschema(name=kmlschemaname)
    schema.newsimplefield(name="address",type="string")

#First, go through each of the points in the jsonpoints object
for point in jsonpoints["features"]:
    #Make a temporary dictionary to hold point attributes for each point in jsonpoints
    newpoint = {}
    #Because there are a few interesting points that do not have addresses, ignore these
    if "Adresse" not in point["properties"]["popup"]:
        continue 
    #Check each point to see if it is a point type of interest
    for pointtype in pointtypes:
        #If the point type is not of interest, ignore it - this needs to happen whether or not splitkml == True
        if point["properties"][pointtype] == False:
            continue
        #Now, all points are of interest and they need to be collected and stored in separate variables that may or may not later be combined based on whether splitkml == True
        #When one of the "point type" properties keys is true, that key's name becomes the value of the new "type" key
        elif point["properties"][pointtype] == True:
            #Set the point type of new point now that we know which key value is True for this point
            newpoint['type'] = pointtype
            break
    #Set the placename of the new point
    newpoint["placename"] = point["properties"]["title"]
        #Note: This came in handy for troubleshooting issues with the address field, as it appears in inconsistent markup within the popup attribute
        #print("\n original popup: ", point["properties"]["popup"])
    #Set the address field, splitting first on </h4><p> or </p><p> and choosing the second slice; and then for the stragglers that continue to contain "</p><h4>", split on that delimeter and choosing the first slice 
    newpoint['address'] = re.split('</p><h4>',re.split(r'</h4><p>|</p><p>', point["properties"]["popup"])[1])[0]
    #Set lat,lng
    newpoint['lat'] = point["geometry"]["coordinates"][1]
    newpoint['lng'] = point["geometry"]["coordinates"][0]
    #Add this point to the list of desired points in the global pointslist
    pointslist.append(newpoint)
    #Also add this point to its corresponding list based on its type - using a little more memory here is easier than chewing up more CPU by filtering by type against global list on the way out to separate the KML files. We already need to have our eyes on each individual point's type here in this loop anyway.
    pointsbytype[newpoint['type']].append(newpoint)
#Now, all points are saved in the pointslist and filterable based on their point type, and now more easily split based on type and saved in separate kml files per type
if splitkml == True:
    #Go through all the point types in the global pointslist
    for pointtype in pointsbytype:
        #Initialize the kml object for this point type
        kmlinit(pointtype)
        #Go through all the points in this point type - this is a bit sloppy and could be made into a function in the future, but it's small and manageable for now
        for point in pointsbytype[pointtype]:
            #Create the point name (placename) and description (use point type as the description) in the kml
            kmlpoint = kml.newpoint(name=point['placename'],description=point['type'])
            #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
            kmlpoint.extendeddata.schemadata.schemaurl = kmlschemaname
            simpledata = kmlpoint.extendeddata.schemadata.newsimpledata('address',point['address'])
            #Finally, add coordinates to the feature
            kmlpoint.coords=[(point['lng'],point['lat'])]
        #Save the kml file for this point type
        kml.save(kmlfile)
else:
    #Initialize the kml object
    kmlinit()
    #Go through all the points in this point type - this is a bit sloppy and could be made into a function in the future, but it's small and manageable for now
    for point in pointslist:
        #Create the point name (placename) and description (use point type as the description) in the kml
        kmlpoint = kml.newpoint(name=point["placename"],description=point["type"])
        #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
        kmlpoint.extendeddata.schemadata.schemaurl = kmlschemaname
        simpledata = kmlpoint.extendeddata.schemadata.newsimpledata("address",point["address"])
        #Finally, add coordinates to the feature
        kmlpoint.coords=[(point["lng"],point["lat"])]
    #Save the kml file
    kml.save(kmlfile)
